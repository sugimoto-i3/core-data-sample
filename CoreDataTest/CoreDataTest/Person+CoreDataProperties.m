//
//  Person+CoreDataProperties.m
//  CoreDataTest
//
//  Created by Itaru on 2016/02/26.
//  Copyright © 2016年 Itaru. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Person+CoreDataProperties.h"

@implementation Person (CoreDataProperties)

@dynamic addres;
@dynamic age;
@dynamic name;
@dynamic id;
@dynamic relationship;

@end
