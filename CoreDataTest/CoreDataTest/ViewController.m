//
//  ViewController.m
//  CoreDataTest
//
//  Created by Itaru on 2016/02/24.
//  Copyright © 2016年 Itaru. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

#define RECORD_NUMBER 10000
#define DATE_FORMAT @"yyyy/MM/dd HH:mm:ss.SSS"

// インスタンス変数
NSDate *startDate;
NSDate *endDate;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.timeLabel.text = @"0秒";
    [self deleteAllRecord];
}

- (IBAction)buttonActionClean:(id)sender {
    [self deleteAllRecord];
    self.timeLabel.text = @"0秒";
}

- (IBAction)insertData {
    // 開始時間
    startDate = [NSDate date];
    
    // context create
    NSManagedObjectContext * context = [[AppDelegate new] managedObjectContext];
    
    NSInteger max = RECORD_NUMBER;
    NSError* error;
    
    for (int i = 0; i < max; i++) {
        NSManagedObject* newObject = [NSEntityDescription insertNewObjectForEntityForName:@"Person"
                                                                   inManagedObjectContext:context];
        NSString* p_id = [NSString stringWithFormat:@"%d", i];
        [newObject setValue: @([p_id intValue])
                 forKeyPath: @"id"];
        [newObject setValue: @"Aisan Taro"
                 forKeyPath: @"name"];
        [newObject setValue: @28
                 forKeyPath: @"age"];
        
        [context save:&error];
    }
    
    // 処理終了位置で現在時間を代入
    endDate = [NSDate date];
    
    // 開始時間と終了時間の差を表示
    NSTimeInterval interval = [endDate timeIntervalSinceDate:startDate];
    NSLog(@"処理開始時間 = %@",[self getDateString:startDate]);
    NSLog(@"処理終了時間 = %@",[self getDateString:endDate]);
    NSLog(@"INSERT処理時間 = %.3f秒",interval);
    self.timeLabel.text = [NSString stringWithFormat:@"INSERT処理時間 = %.3f秒",interval];
    
    
}

- (IBAction)updateData:(id)sender {
    // 開始時間
    startDate = [NSDate date];
    
    // context create
    NSManagedObjectContext * context = [[AppDelegate new] managedObjectContext];
    
    NSInteger max = RECORD_NUMBER;
    NSError* error;
    // NSFetchRequestは、検索条件などを保持させるオブジェクトです。
    // 後続処理では、このインスタンスに色々と検索条件を設定します。
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // 検索対象のエンティティを指定します。
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // 一度に読み込むサイズを指定します。
    [fetchRequest setFetchBatchSize:20];
    
    // 検索結果を保持する順序を指定します。
    // ここでは、keyというカラムの値の昇順で保持するように指定しています。
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    // NSFetchedResultsControllerを作成します。
    // 上記までで作成したFetchRequestを指定します。
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                               managedObjectContext:context
                                                                                                 sectionNameKeyPath:nil
                                                                                                          cacheName:nil];
    
    for (int i = 0; i < max; i++) {
        NSManagedObject *updObject = [fetchedResultsController.fetchedObjects objectAtIndex:i];
        [updObject setValue: @"Aisan Jiro"
                 forKeyPath: @"name"];
        [updObject setValue: @27
                 forKeyPath: @"age"];
        
        [context save:&error];
    }
    
    NSError *saveError = nil;
    
    //削除を反映
    [context save:&saveError];
    
    // 処理終了位置で現在時間を代入
    endDate = [NSDate date];
    
    // 開始時間と終了時間の差を表示
    NSTimeInterval interval = [endDate timeIntervalSinceDate:startDate];
    NSLog(@"処理開始時間 = %@",[self getDateString:startDate]);
    NSLog(@"処理終了時間 = %@",[self getDateString:endDate]);
    NSLog(@"UPDATE処理時間 = %.3f秒",interval);
    self.timeLabel.text = [NSString stringWithFormat:@"UPDATE処理時間 = %.3f秒",interval];
}

- (IBAction)getData {
    // 開始時間
    startDate = [NSDate date];
    
    // NSFetchRequestは、検索条件などを保持させるオブジェクトです。
    // 後続処理では、このインスタンスに色々と検索条件を設定します。
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext * context = [[AppDelegate new] managedObjectContext];
    // 検索対象のエンティティを指定します。
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // 一度に読み込むサイズを指定します。
    //[fetchRequest setFetchBatchSize:20];
    
    // 検索結果を保持する順序を指定します。
    // ここでは、keyというカラムの値の昇順で保持するように指定しています。
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    // NSFetchedResultsControllerを作成します。
    // 上記までで作成したFetchRequestを指定します。
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                               managedObjectContext:context
                                                                                                 sectionNameKeyPath:nil
                                                                                                          cacheName:nil];
    
    // データ検索を行います。
    // 失敗した場合には、メソッドはfalseを返し、引数errorに値を詰めてくれます。
    NSError *error = nil;
    if (![fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    
    // 検索結果をコンソールに出力してみます。
    // fetchedObjectsというメソッドで、検索結果一覧を配列で受け取れます。
    NSArray *moArray = [fetchedResultsController fetchedObjects];
    for (int i = 0; i < moArray.count; i++) {
        NSManagedObject *object = [moArray objectAtIndex:i];
        NSString *p_id  = [object valueForKey:@"id"];
        NSString *key   = [object valueForKey:@"age"];
        NSString *value = [object valueForKey:@"name"];
    }
    
    // 処理終了位置で現在時間を代入
    endDate = [NSDate date];
    
    // 開始時間と終了時間の差を表示
    NSTimeInterval interval = [endDate timeIntervalSinceDate:startDate];
    NSLog(@"処理開始時間 = %@",[self getDateString:startDate]);
    NSLog(@"処理終了時間 = %@",[self getDateString:endDate]);
    NSLog(@"SELECT処理時間 = %.3f秒",interval);
    
    self.timeLabel.text = [NSString stringWithFormat:@"SELECT処理時間 = %.3f秒",interval];
}

- (void)deleteAllRecord {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext * context = [[AppDelegate new] managedObjectContext];
    
    // 検索対象のエンティティを指定します。
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // 検索結果を保持する順序を指定します。
    // ここでは、keyというカラムの値の昇順で保持するように指定しています。
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    // NSFetchedResultsControllerを作成します。
    // 上記までで作成したFetchRequestを指定します。
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                               managedObjectContext:context
                                                                                                 sectionNameKeyPath:nil
                                                                                                          cacheName:nil];
    
    // データ検索を行います。
    // 失敗した場合には、メソッドはfalseを返し、引数errorに値を詰めてくれます。
    NSError *error = nil;
    if (![fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    // 検索結果をコンソールに出力してみます。
    // fetchedObjectsというメソッドで、検索結果一覧を配列で受け取れます。
    NSArray *moArray = [fetchedResultsController fetchedObjects];
    for (int i = 0; i < moArray.count; i++) {
        [context deleteObject:[moArray objectAtIndex:i]];
    }
    
    NSError *saveError = nil;
    
    //削除を反映
    [context save:&saveError];
}

/**
 *  日付をミリ秒までの表示にして文字列で返す
 *
 *  @param date データ
 *
 *  @return 時間
 */
- (NSString*)getDateString:(NSDate*)date
{
    // 日付フォーマットオブジェクトの生成
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    // フォーマットを指定の日付フォーマットに設定
    [dateFormatter setDateFormat:DATE_FORMAT];
    // 日付の文字列を生成
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
