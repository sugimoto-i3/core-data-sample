//
//  ViewController.h
//  CoreDataTest
//
//  Created by Itaru on 2016/02/24.
//  Copyright © 2016年 Itaru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel* timeLabel;

@end

