//
//  Person+CoreDataProperties.h
//  CoreDataTest
//
//  Created by Itaru on 2016/02/26.
//  Copyright © 2016年 Itaru. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Person.h"

NS_ASSUME_NONNULL_BEGIN

@interface Person (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *addres;
@property (nullable, nonatomic, retain) NSNumber *age;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSSet<Person *> *relationship;

@end

@interface Person (CoreDataGeneratedAccessors)

- (void)addRelationshipObject:(Person *)value;
- (void)removeRelationshipObject:(Person *)value;
- (void)addRelationship:(NSSet<Person *> *)values;
- (void)removeRelationship:(NSSet<Person *> *)values;

@end

NS_ASSUME_NONNULL_END
